using Microsoft.Playwright;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Test.DentcamCRM.Pages
{
    public class PriceTests
    {
        private const string TESTING_GROUP_NAME = "TestingPriceGroup";
        private const string TESTING_ITEM_NAME = "testingItemName";
        private const string TESTING_ITEM_CODE = "testingItemCode";
        private const string TESTING_ITEM_PRICE = "100";
        private const string TESTING_GROUP_NAME_EDITED = "TestingPriceGroupEdited";
        private const string TESTING_ITEM_NAME_EDITED = "testingItemNameEdited";
        private const string TESTING_ITEM_CODE_EDITED = "testingItemCodeEd";
        private const string TESTING_ITEM_PRICE_EDITED = "200";

        [Fact]
        public async Task CheckPriceCreateGroupCreateItems()
        {
            using var playwright = await Playwright.CreateAsync();
            await using var browser = await playwright.Chromium.LaunchAsync(new BrowserTypeLaunchOptions
            {
                Headless = false,
            });
            var context = await browser.NewContextAsync();

            IPage? page = await context.NewPageAsync();

            await page.GotoAsync($"{TestsHelper.TESTING_URL}auth");

            await page.FillAsync("#Inputlogin", TestsHelper.LOGIN);

            await page.FillAsync("#Inputpassword", TestsHelper.PASSWORD);

            await page.RunAndWaitForNavigationAsync(async () =>
            {
                await page.ClickAsync("#login-button");
            });

            bool loginPassed = page.Url.Equals($"{TestsHelper.TESTING_URL}schedule");

            if (loginPassed)
            {
                await page.RunAndWaitForNavigationAsync(async () =>
                {
                    await page.ClickAsync("#menu_price");
                });
                await page.WaitForSelectorAsync(".nav-tabs");

                bool newGroupCreated = CheckNewPriceGroupAddedAsync(page).Result;

                //New price group created.
                Assert.True(newGroupCreated);

                if (newGroupCreated)
                {
                    bool newGroupEdited = CheckNewPriceGroupEditedAsync(page).Result;

                    //New price group edited.
                    Assert.True(newGroupEdited);

                    bool newPriceItemAdded = CheckNewPriceItemAddedAsync(page).Result;

                    //New price item added.
                    Assert.True(newPriceItemAdded);

                    if (newPriceItemAdded)
                    {
                        bool newPriceItemEdited = CheckNewPriceItemEditedAsync(page).Result;

                        //New price item edited
                        Assert.True(newPriceItemEdited);

                        bool newPriceItemDeleted = CheckNewPriceItemDeletedAsync(page).Result;

                        //New price item deleted.
                        Assert.True(newPriceItemDeleted);
                    }
                }

                bool testingGroupDeletedAfterTest = CheckTestingGroupDeletedAsync(page).Result;
                //Testing group deleted after test.
                Assert.True(testingGroupDeletedAfterTest);
            }

        }

        private static async Task<bool> CheckTestingGroupDeletedAsync(IPage? page)
        {
            if (page == null)
                return false;
            bool testingGroupExists = TestsHelper.ElementsContentMatchingSelector(".nav-item", page)
                                    .Result
                                    .Contains(TESTING_GROUP_NAME_EDITED);
            if (!testingGroupExists)
                return true;

            await page.RunAndWaitForResponseAsync(async () =>
            {
                await page.ClickAsync($"text={TESTING_GROUP_NAME_EDITED}");
            }, (resp) => resp.Url.Contains("treat_card/get_prices"));

            await page.ClickAsync(".dropdown-toggle");
            await page.ClickAsync("#deltab");

            await page.RunAndWaitForResponseAsync(async () =>
            {
                await page.ClickAsync("#submit_button");
            }, (resp) => resp.Url.Contains("price/processitem"));

            testingGroupExists = TestsHelper.ElementsContentMatchingSelector(".nav-item", page)
                                            .Result
                                            .Contains(TESTING_GROUP_NAME_EDITED);
            //Testing group deleted.
            bool deleteGroupPassed = !testingGroupExists;

            return deleteGroupPassed;
        }

        private static async Task<bool> CheckNewPriceGroupAddedAsync(IPage? page)
        {
            if (page == null)
                return false;

            int priceGroupsCountBefore = TestsHelper.ElementsFoundMatchingSelector(".nav-item", page).Result;

            await page.ClickAsync(".dropdown-toggle");
            await page.ClickAsync("#newtab");
            await page.FillAsync("#gr_itm_name", TESTING_GROUP_NAME);

            await page.RunAndWaitForResponseAsync(async () =>
            {
                await page.ClickAsync("#submit_button");
            }, (resp) => resp.Url.Contains("price/processitem"));

            int priceGroupsCountAfter = TestsHelper.ElementsFoundMatchingSelector(".nav-item", page).Result;

            // Testing group added.
            bool addGroupPassed = priceGroupsCountAfter == priceGroupsCountBefore + 1;

            bool testresult = addGroupPassed;

            return testresult;
        }

        private static async Task<bool> CheckNewPriceGroupEditedAsync(IPage? page)
        {
            if (page == null)
                return false;

            bool testingGroupExists = TestsHelper.ElementsContentMatchingSelector(".nav-item", page)
                                    .Result
                                    .Contains(TESTING_GROUP_NAME);
            if (!testingGroupExists)
                return true;

            await page.RunAndWaitForResponseAsync(async () =>
            {
                await page.ClickAsync($"text={TESTING_GROUP_NAME}");
            }, (resp) => resp.Url.Contains("treat_card/get_prices"));

            await page.ClickAsync(".dropdown-toggle");
            await page.ClickAsync("#edittab");
            await page.FillAsync("#gr_itm_name", TESTING_GROUP_NAME_EDITED);

            await page.RunAndWaitForResponseAsync(async () =>
            {
                await page.ClickAsync("#submit_button");
            }, (resp) => resp.Url.Contains("price/processitem"));

            // Testing group edited.
            bool testresult = TestsHelper.ElementsContentMatchingSelector(".nav-item", page)
                                    .Result
                                    .Contains(TESTING_GROUP_NAME_EDITED);

            return testresult;
        }
        private static async Task<bool> CheckNewPriceItemAddedAsync(IPage? page)
        {
            if (page == null)
                return false;

            bool testingGroupExists = TestsHelper.ElementsContentMatchingSelector(".nav-item", page)
                                    .Result
                                    .Contains(TESTING_GROUP_NAME_EDITED);
            if (!testingGroupExists)
                return true;

            await page.RunAndWaitForResponseAsync(async () =>
            {
                await page.ClickAsync($"text={TESTING_GROUP_NAME_EDITED}");
            }, (resp) => resp.Url.Contains("treat_card/get_prices"));

            await page.ClickAsync(".add-price-item-button");
            await page.FillAsync("#gr_itm_name", TESTING_ITEM_NAME);
            await page.FillAsync("#itm_code", TESTING_ITEM_CODE);
            await page.FillAsync("#itm_price", TESTING_ITEM_PRICE);

            await page.RunAndWaitForResponseAsync(async () =>
            {
                await page.ClickAsync("#submit_button");
            }, (resp) => resp.Url.Contains("treat_card/get_prices"));

            await page.RunAndWaitForResponseAsync(async () =>
            {
                await page.ClickAsync($"text={TESTING_GROUP_NAME_EDITED}");
            }, (resp) => resp.Url.Contains("treat_card/get_prices"));

            int itemsAddedCount = TestsHelper.ElementsFoundMatchingSelector("#prices_tab > tbody > tr", page).Result;

            //New price item added.
            bool testresult = itemsAddedCount == 1;

            return testresult;
        }

        private static async Task<bool> CheckNewPriceItemEditedAsync(IPage? page)
        {
            if (page == null)
                return false;
            bool testresult = false;

            bool testingGroupExists = TestsHelper.ElementsContentMatchingSelector(".nav-item", page)
                        .Result
                        .Contains(TESTING_GROUP_NAME_EDITED);
            if (!testingGroupExists)
                return false;

            await page.RunAndWaitForResponseAsync(async () =>
            {
                await page.ClickAsync($"text={TESTING_GROUP_NAME_EDITED}");
            }, (resp) => resp.Url.Contains("treat_card/get_prices"));

            int itemsAddedCount = TestsHelper.ElementsFoundMatchingSelector("#prices_tab > tbody > tr", page).Result;
            if (itemsAddedCount == 1)
            {
                await page.ClickAsync("#prices_tab > tbody > tr");
                await page.ClickAsync("#prices_tab .edit-item-button");
                await page.FillAsync("#gr_itm_name", TESTING_ITEM_NAME_EDITED);
                await page.FillAsync("#itm_code", TESTING_ITEM_CODE_EDITED);
                await page.FillAsync("#itm_price", TESTING_ITEM_PRICE_EDITED);

                await page.RunAndWaitForResponseAsync(async () =>
                {
                    await page.ClickAsync("#submit_button");
                }, (resp) => resp.Url.Contains("treat_card/get_prices"));

                await page.RunAndWaitForResponseAsync(async () =>
                {
                    await page.ClickAsync($"text={TESTING_GROUP_NAME_EDITED}");
                }, (resp) => resp.Url.Contains("treat_card/get_prices"));

                await page.ClickAsync("#prices_tab > tbody > tr");
                string[] itemEditedContext = TestsHelper.ElementsContentMatchingSelector("#prices_tab > tbody > tr", page).Result;
                string editedContext = string.Join(',', itemEditedContext);

                //Testing item edited.
                testresult = editedContext.Contains(TESTING_ITEM_NAME_EDITED) && editedContext.Contains(TESTING_ITEM_CODE_EDITED);
            }
            return testresult;
        }
        private static async Task<bool> CheckNewPriceItemDeletedAsync(IPage? page)
        {
            if (page == null)
                return false;

            bool testingGroupExists = TestsHelper.ElementsContentMatchingSelector(".nav-item", page)
            .Result
            .Contains(TESTING_GROUP_NAME_EDITED);
            if (!testingGroupExists)
                return false;

            await page.RunAndWaitForResponseAsync(async () =>
            {
                await page.ClickAsync($"text={TESTING_GROUP_NAME_EDITED}");
            }, (resp) => resp.Url.Contains("treat_card/get_prices"));

            int itemsCountBeforeDelete = TestsHelper.ElementsFoundMatchingSelector("#prices_tab > tbody > tr", page).Result;
            if (itemsCountBeforeDelete == 1)
            {
                await page.ClickAsync("#prices_tab > tbody > tr");
                await page.ClickAsync("#prices_tab .delete-item-button");

                await page.RunAndWaitForResponseAsync(async () =>
                {
                    await page.ClickAsync("#submit_button");
                }, (resp) => resp.Url.Contains("treat_card/get_prices"));

                await page.RunAndWaitForResponseAsync(async () =>
                {
                    await page.ClickAsync($"text={TESTING_GROUP_NAME_EDITED}");
                }, (resp) => resp.Url.Contains("treat_card/get_prices"));

            }
            string[] itemContext = TestsHelper.ElementsContentMatchingSelector("#prices_tab > tbody > tr", page).Result;
            int itemsCountAfterDelete = itemContext.Length;

            // Testing item deleted.
            bool testresult = itemsCountAfterDelete == 1;
            return testresult;
        }

    }
}
