using Microsoft.Playwright;
using System.Threading.Tasks;
using Xunit;

namespace Test.DentcamCRM.Pages
{
    public class LoginTests
    {

        [Fact]
        public async Task CheckLogin()
        {
            using var playwright = await Playwright.CreateAsync();
            await using var browser = await playwright.Chromium.LaunchAsync(new BrowserTypeLaunchOptions
            {
                Headless = false,
            });
            var context = await browser.NewContextAsync();

            IPage? page = await context.NewPageAsync();

            await page.GotoAsync($"{TestsHelper.TESTING_URL}auth");

            await page.FillAsync("#Inputlogin", TestsHelper.LOGIN);

            await page.FillAsync("#Inputpassword", TestsHelper.PASSWORD);

            await page.RunAndWaitForNavigationAsync(async () =>
                {
                    await page.ClickAsync("#login-button");
                });


            //Checks login page passed.
            Assert.Equal($"{TestsHelper.TESTING_URL}schedule", page.Url);
        }
    }
}
