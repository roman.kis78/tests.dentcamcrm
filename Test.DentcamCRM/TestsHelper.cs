using Microsoft.Playwright;
using System;
using System.Threading.Tasks;

namespace Test.DentcamCRM
{
    public class TestsHelper
    {
        public const string TESTING_URL = "https://develop.dentcamcrm.com/";
        public const string PASSWORD = "password";
        public const string LOGIN = "kravol";
        public const int WAIT_SELECTOR_INTERVAL = 5000;
        public static async Task<int> ElementsFoundMatchingSelector(string selector, IPage? page)
        {
            IElementHandle? elementsfound;
            int elementscount = 0;
            if (page != null)
                try
                {
                    elementsfound = await page.WaitForSelectorAsync(selector, new PageWaitForSelectorOptions()
                    {
                        Timeout = WAIT_SELECTOR_INTERVAL,
                        State = WaitForSelectorState.Attached
                    });
                    string[] elements = Array.Empty<string>();
                    elements = await page.EvalOnSelectorAllAsync<string[]>(selector, "nodes => nodes.map(n => n.innerText)");
                    elementscount = elements.Length;
                }
                catch (TimeoutException)
                {
                    return elementscount;
                }

            return elementscount;
        }

        public static async Task<string[]> ElementsContentMatchingSelector(string selector, IPage? page)
        {
            IElementHandle? elementsfound;
            string[] elements = Array.Empty<string>();
            if (page != null)
                try
                {
                    elementsfound = await page.WaitForSelectorAsync(selector, new PageWaitForSelectorOptions()
                    {
                        Timeout = WAIT_SELECTOR_INTERVAL,
                        State = WaitForSelectorState.Attached
                    });
                    
                    elements = await page.EvalOnSelectorAllAsync<string[]>(selector, "nodes => nodes.map(n => n.innerText)");
                    int elementscount = elements.Length;
                }
                catch (TimeoutException)
                {
                    return elements;
                }

            return elements;
        }
    }
}
